﻿using AutoMapper;
using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<Franchise, FranchiseCreateDTO>();
            CreateMap<Franchise, FranchiseEditDTO>();
        }
    }
}
