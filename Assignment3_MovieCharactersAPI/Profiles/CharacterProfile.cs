﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using Assignment3_MovieCharactersAPI.Models.DTO.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<Character, CharacterCreateDTO>();
            CreateMap<Character, CharacterEditDTO>();
        }
    }
}
