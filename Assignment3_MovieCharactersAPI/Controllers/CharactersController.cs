﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Services;
using AutoMapper;
using Assignment3_MovieCharactersAPI.Models.DTO;
using Assignment3_MovieCharactersAPI.Models.DTO.Character;
using System.Net.Mime;

namespace Assignment3_MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _service;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns>Returns list of characters</returns>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAll()
        {
            var all = await _service.GetAllCharacters();
            return _mapper.Map<List<CharacterReadDTO>>(all);
        }

        /// <summary>
        /// Gets character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns character</returns>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> Get(int id)
        {
            var character = await _service.Get(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Updates character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Returns updated character</returns>
        // PUT: api/Characters/5
        [HttpPut]
        public async Task<ActionResult<CharacterEditDTO>> PutCharacter(Character character)
        {
            var updtCharacter = await _service.Update(character);

            return _mapper.Map<CharacterEditDTO>(updtCharacter);
        }
        
        /// <summary>
        /// Updates characters to movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns>Returns true or false</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> PutCharacter(int id, int[] characterIds)
        {
            var boolVal = await _service.Update(id, characterIds);

            return boolVal;
        }

        /// <summary>
        /// Creates character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Returns added character</returns>
        // POST: api/Characters
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter(Character character)
        {
            var updtCharacter = await _service.Create(character);

            return _mapper.Map<CharacterCreateDTO>(updtCharacter);
        }

        /// <summary>
        /// Deletes character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns true or false</returns>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<bool> DeleteCharacter(int id)
        {
            var boolVal = await _service.Delete(id);

            return boolVal;
        }

    }
}
