﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_MovieCharactersAPI.Models;
using AutoMapper;
using Assignment3_MovieCharactersAPI.Services;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System.Net.Mime;

namespace Assignment3_MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _service;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all francises
        /// </summary>
        /// <returns>Returns list of franchises</returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<List<FranchiseReadDTO>>> GetAll()
        {
            var all = await _service.GetAllFranchise();
            return _mapper.Map<List<FranchiseReadDTO>>(all);
        }

        /// <summary>
        /// Gets franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns francise</returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> Get(int id)
        {
            var franchise = await _service.Get(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Updates francise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Returns updated franchise</returns>
        // PUT: api/Franchises/5
        [HttpPut]
        public async Task<ActionResult<FranchiseEditDTO>> PutFranchise(Franchise franchise)
        {
            var updtFranchise = await _service.Update(franchise);

            return _mapper.Map<FranchiseEditDTO>(updtFranchise);
        }

        /// <summary>
        /// Updates movies to franchise
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="franchiseId"></param>
        /// <returns>Returns true or false</returns>
        [HttpPut("{franchiseId}")]
        public async Task<ActionResult<bool>> PutFranchise(int franchiseId, int[] movieIds)
        {
            var boolVal = await _service.Update(movieIds, franchiseId);
            return boolVal;
        }

        /// <summary>
        /// Creates new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Returns created franchise</returns>
        // POST: api/Franchises
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise(Franchise franchise)
        {
            var updtFranchise = await _service.Create(franchise);

            return _mapper.Map<FranchiseCreateDTO>(updtFranchise);
        }

        /// <summary>
        /// Deletes franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns true or false</returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<bool> DeleteFranchise(int id)
        {
            var boolVal = await _service.Delete(id);

            return boolVal;
        }
    }
}
