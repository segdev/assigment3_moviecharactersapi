﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Services;
using AutoMapper;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System.Net.Mime;

namespace Assignment3_MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _service;
        private readonly IMapper _mapper;

        public MoviesController(
            IMovieService service, 
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies from database
        /// </summary>
        /// <returns>Returns list of movies</returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<List<MovieReadDTO>>> GetAll()
        {
            var all = await _service.GetAllMovies();
            return _mapper.Map<List<MovieReadDTO>>(all);
        }

        /// <summary>
        /// Gets movie by id from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns movie</returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> Get(int id)
        {
            var movie = await _service.Get(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Updates movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>Returns updated movie</returns>
        // PUT: api/Movies/5
        [HttpPut("{id}")]
        public async Task<ActionResult<MovieEditDTO>> PutMovie(int id, Movie movie)
        {
            var updatedMovie = await _service.Update(movie);

            return _mapper.Map<MovieEditDTO>(updatedMovie);
        }

        /// <summary>
        /// Adds new movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Returns created movie</returns>
        // POST: api/Movies
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie(Movie movie)
        {
            var addedMovie = await _service.Create(movie);

            return _mapper.Map<MovieCreateDTO>(addedMovie);
        }

        /// <summary>
        /// Deletes movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns true or false</returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<bool> DeleteMovie(int id)
        {
            return await _service.Delete(id);
           
        }

    }
}
