﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public interface ICharacterService
    {
        public Task<bool> Update(int movieId, int[] characterIds);
        public Task<List<Character>> GetAllByMovie(int movieId);
        public Task<List<Character>> GetAllByFranchise(int franchiseId);      
        public Task<Character> Create(Character character);
        public Task<Character> Get(int characterId);
        public Task<List<Character>> GetAllCharacters();
        public Task<Character> Update(Character character);
        public Task<bool> Delete(int characterId);
    }
}
