﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;
        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Returns movie</returns>
        public async Task<Movie> Create(Movie movie)
        {
            var resultMovie = _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return resultMovie.Entity;
        }

        /// <summary>
        /// Delete movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>Returns true or false</returns>
        public async Task<bool> Delete(int movieId)
        {
            Movie movie = await _context.Movies.FindAsync(movieId);
            
            if (movie != null)
            {
                _context.Movies.Remove(movie);
                await _context.SaveChangesAsync();
                return true;
            }

            return false;

        }

        /// <summary>
        /// Get movie by id
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>Returns movie</returns>
        public async Task<Movie> Get(int movieId)
        {
            return await _context.Movies.FindAsync(movieId);
        }

        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns>Returns list of movies</returns>
        public async Task<List<Movie>> GetAllMovies()
        {
            return await _context.Movies.ToListAsync();
        }

        /// <summary>
        /// Update movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Returns movie</returns>
        public async Task<Movie> Update(Movie movie)
        {
            var resultMovie = _context.Update(movie);
            await _context.SaveChangesAsync();

            return resultMovie.Entity;

        }
    }
}
