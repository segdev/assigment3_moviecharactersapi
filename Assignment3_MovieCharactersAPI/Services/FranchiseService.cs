﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;
        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Returns franchise</returns>
        public async Task<Franchise> Create(Franchise franchise)
        {
            var returnValue = _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();


            return returnValue.Entity;
        }

        /// <summary>
        /// Delete franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>Returns true or false</returns>
        public async Task<bool> Delete(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
                return false;
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Get franchise by id
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>Returns franchise</returns>
        public async Task<Franchise> Get(int franchiseId)
        {
            return await _context.Franchises.SingleOrDefaultAsync(s => s.Id == franchiseId);
        }

        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns>Returns list of franchises</returns>
        public async Task<List<Franchise>> GetAllFranchise()
        {
            return await _context.Franchises.ToListAsync();
        }

        /// <summary>
        /// Get all movies by franchise id
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>Returns list of movies</returns>
        public async Task<List<Movie>> GetAllMovies(int franchiseId)
        {
            return await _context.Movies.Where(n => n.FranchiseId == franchiseId).ToListAsync();
        }

        /// <summary>
        /// Update franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Returns franchise</returns>
        public async Task<Franchise> Update(Franchise franchise)
        {
            var returnValue = _context.Update(franchise);

            await _context.SaveChangesAsync();

            return returnValue.Entity;
        }

        /// <summary>
        /// Updates movies to franchise
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="franchiseId"></param>
        /// <returns>Returns true or false</returns>
        public async Task<bool> Update(int[] movieIds, int franchiseId)
        {
            Franchise franchiseToUpdate = await _context.Franchises.Include(m => m.Movies).Where(d => d.Id == franchiseId).FirstOrDefaultAsync();
            List<Movie> movieList = await _context.Movies.Where(s => movieIds.Contains(s.Id)).ToListAsync();

            foreach(var movie in movieList)
            {
                movie.FranchiseId = franchiseToUpdate.Id;
            }

            _context.UpdateRange(movieList);

            int entries = await _context.SaveChangesAsync();

            return entries > 0 ? true : false;

        }
    }
}
