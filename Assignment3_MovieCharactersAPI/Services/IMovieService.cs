﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public interface IMovieService
    {
        public Task<Movie> Update(Movie movie);
        public Task<List<Movie>> GetAllMovies();
        public Task<Movie> Create(Movie movie);
        public Task<Movie> Get(int movieId);
        public Task<bool>Delete(int movieId);
    }
}
