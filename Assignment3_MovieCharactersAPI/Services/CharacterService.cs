﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;
        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates new character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Returns character</returns>
        public async Task<Character> Create(Character character)
        {
            var returnValue = _context.Characters.Add(character);
            await _context.SaveChangesAsync();


            return returnValue.Entity;
        }

        /// <summary>
        /// Deletes character
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>Retursn true or false</returns>
        public async Task<bool> Delete(int characterId)
        {
            var character = await _context.Characters.FindAsync(characterId);
            if (character == null)
                return false;
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            return true;

        }

        /// <summary>
        /// Gets character by id
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>REturns character</returns>
        public async Task<Character> Get(int characterId)
        {
            return await _context.Characters.SingleOrDefaultAsync(s => s.Id == characterId);
        }


        /// <summary>
        /// Get all characters by franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>Returns list of characters</returns>
        public async Task<List<Character>> GetAllByFranchise(int franchiseId)
        {
            return await _context.Characters.Include(s => s.Movies).ThenInclude(m => m.Franchise).Where(n => n.Id == franchiseId).ToListAsync();
        }

        /// <summary>
        /// Get all characters by movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>Returns list of characters</returns>
        public async Task<List<Character>> GetAllByMovie(int movieId)
        {
            return await _context.Characters.Include(s => s.Movies).Where(n => n.Id == movieId).ToListAsync();
        }

        /// <summary>
        /// Get all characters
        /// </summary>
        /// <returns>Returns list of characters</returns>
        public async Task<List<Character>> GetAllCharacters()
        {
            return await _context.Characters.ToListAsync();
        }

        /// <summary>
        /// Update characters to movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns>Returns true or false</returns>
        public async Task<bool> Update(int movieId, int[] characterIds)
        {

            Movie movieToUpdateCharacters = await _context.Movies.Include(c => c.Characters).Where(m => m.Id == movieId).FirstOrDefaultAsync();

            if (movieToUpdateCharacters == null)
                return false;

            ICollection<Character> charactersList = await _context.Characters.Where(s => characterIds.Contains(s.Id)).ToListAsync();

            movieToUpdateCharacters.Characters = charactersList;
            _context.Entry(movieToUpdateCharacters).State = EntityState.Modified;

            int entries = await _context.SaveChangesAsync();

            return entries > 0 ? true : false;
        }

        /// <summary>
        /// Update character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Returns character</returns>
        public async Task<Character> Update(Character character)
        {
            var returnValue = _context.Update(character);

            await _context.SaveChangesAsync();

            return returnValue.Entity;
        }
    }
}
