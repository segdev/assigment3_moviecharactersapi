﻿using Assignment3_MovieCharactersAPI.Models;
using Assignment3_MovieCharactersAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Services
{
    public interface IFranchiseService
    {
        public Task<Franchise> Create(Franchise franchise);
        public Task<Franchise> Get(int franchiseId);
        public Task<List<Franchise>> GetAllFranchise();
        public Task<Franchise> Update(Franchise franchise);
        public Task<bool> Update(int[] movieIds, int franchiseId);
        public Task<bool> Delete(int franchiseId);
        public Task<List<Movie>> GetAllMovies(int franchiseId);


    }
}
