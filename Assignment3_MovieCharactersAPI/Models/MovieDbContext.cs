﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        private IConfiguration configuration;

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public object Franchise { get; internal set; }

        public MovieDbContext(IConfiguration config)
        {
            configuration = config;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connStr = configuration.GetSection("ConnectionStrings").GetSection("localSQLMovieDB").Value;
            optionsBuilder.UseSqlServer(connStr);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    Id = 1,
                    Name = "Horror Streak",
                    Description = "Only horrors allowed",
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Comedy Streak",
                    Description = "Only comedy allowed",
                },
                new Franchise()
                {
                    Id = 3,
                    Name = "Action Friday",
                    Description = "Only action allowed",
                }
            );

            modelBuilder.Entity<Movie>().HasData(
                new Movie(){
                    Id = 1,
                    Title = "Tittelituure",
                    Genre = "Horror",
                    ReleaseYear = 2000,
                    Director = "Petteri Puikelo",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia",
                    TrailerUrl = "www.urlutinkuvahommelo.fi/traileri",
                    FranchiseId = 1
                },new Movie(){
                    Id = 2,
                    Title = "Huihaihai",
                    Genre = "Action",
                    ReleaseYear = 2020,
                    Director = "Kuikka Nipparinen",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia",
                    TrailerUrl = "www.urlutinkuvahommelo.fi/traileri",
                    FranchiseId = 3
                },new Movie(){
                    Id = 3,
                    Title = "Haha Hauska Mummo",
                    Genre = "Comedy",
                    ReleaseYear = 2021,
                    Director = "Aliisa Mummola",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia",
                    TrailerUrl = "www.urlutinkuvahommelo.fi/traileri",
                    FranchiseId = 2
                }
            );

            modelBuilder.Entity<Character>().HasData(
                new Character(){
                    Id = 1,
                    FullName = "Senor Pablo",
                    Alias = "The Pablo",
                    Gender = "Male",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia/pablo",

                }, new Character(){
                    Id = 2,
                    FullName = "Senorita Riitta",
                    Alias = "Riiitta",
                    Gender = "Female",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia/riiitta",

                }, new Character(){
                    Id = 3,
                    FullName = "Juha Tapiola",
                    Alias = "Tapsa",
                    Gender = "Male",
                    PictureUrl = "www.urlutinkuvahommelo.fi/kuvia/tapsa",

                }
            );

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 3 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 3, MovieId = 2 },
                            new { CharacterId = 3, MovieId = 3 }
                        );
                    });

        }



    }
}
