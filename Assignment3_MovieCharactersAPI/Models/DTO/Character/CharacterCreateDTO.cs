﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterCreateDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
}
