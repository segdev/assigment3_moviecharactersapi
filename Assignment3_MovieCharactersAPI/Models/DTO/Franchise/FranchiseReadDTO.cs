﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models.DTO
{
    public class FranchiseReadDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
