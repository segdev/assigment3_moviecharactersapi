﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models.DTO
{
    public class FranchiseCreateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
