﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        public string PictureUrl { get; set; }
        public ICollection<Movie> Movies { get; set; }


    }
}
