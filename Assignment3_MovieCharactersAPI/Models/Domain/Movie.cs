﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_MovieCharactersAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [MaxLength(20)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string TrailerUrl { get; set; }
        public ICollection<Character> Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

    }
}
