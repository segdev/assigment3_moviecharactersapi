# Assignment 3

Create an Entity Framework Code First workflow and an ASP.NET Core Web API.

Project contains
- Asp.net web API with create, read, update and delete functionality for each model
- Migrations with seeded data
- Swagger documentation
- [Database Diagram](https://gitlab.com/segdev/assigment3_moviecharactersapi/-/blob/master/Assignment3_MovieCharactersAPI/UML.PNG)

